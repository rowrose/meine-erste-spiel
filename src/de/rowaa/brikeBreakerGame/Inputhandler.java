package de.rowaa.brikeBreakerGame;


import de.rowaa.brikeBreakerGame.model.Model;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class Inputhandler {

    private Model model;
    private Timer timer;
    private Graphics graphics;


    public Inputhandler(Model model){
        this.model = model;
    }

    private int counter = 0;

    public void keyPressed(KeyEvent e) {

        timer = new Timer( model, graphics);

        if (e.getCode() == KeyCode.RIGHT){
            if (model.getSpielerX() >=982){
                int spieler = model.getSpielerX();
                spieler = 983;
            }else {
                model.richtBewegen(1/4);
            }
        }
        if (e.getCode() == KeyCode.LEFT){
            if (model.getSpielerX() < 10){
                int spieler = model.getSpielerX();
                spieler = 10;
            }else {
                model.linksBewegen(1/4)  ;
            }
        }

        if (e.getCode()== KeyCode.ENTER){
          model.reset();
        }
    }
}


