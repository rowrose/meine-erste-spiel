package de.rowaa.brikeBreakerGame.model;


import java.util.LinkedList;
import java.util.List;

public class Model {

    private boolean spiel = false;
    private boolean gameOver = false;
    private boolean winner = false;
    private int score = 0;
    private int spielerX = 480;
    private float ballPosX = 350;
    private float ballPosY = 500;
    private int ballRichtungX = -1;
    private int ballRichtungY = -3;



    private List<Stein> steine = new LinkedList<>();


    public Model() {
        reset();
    }

    public void reset() {

        gameOver = false;
        winner = false;


        if (!spiel){
            spiel = true;
             score = 0;
             spielerX = 480;
             ballPosX =350;
            ballPosY = 400;
             ballRichtungX = -1;
             ballRichtungY = -3;

        }
        steine.clear();

        for (int i = 100; i < 1100; i += 70) {
            steine.add( new Stein( i, 150) );

        }
        for (int i = 130; i < 1050; i += 70) {
            steine.add( new Stein( i, 110) );

        }
        for (int i = 70; i < 1100; i += 70) {
            steine.add( new Stein( i, 190) );

        }
    }

    private int counter = 0;

    public void update(long deltMillis) {
        counter += deltMillis;

        if (spiel) {

            ballPosX += ballRichtungX * deltMillis / 08.0;
            if (ballPosX <= 5) {
                ballRichtungX = -ballRichtungX;
            }
            if (ballPosX >= 1194) {
                ballRichtungX = -ballRichtungX;
            }


            if (ballPosY >= 650 && ballPosY < 680 && ballPosX >= spielerX-20 && ballPosX <= spielerX + 210) {
                ballRichtungY = -ballRichtungY-1;

            }


            for (Stein stein : getSteine()) {
                if (ballPosX >= stein.getStPosX() && ballPosX <stein.getStPosX()+60 && ballPosY>= stein.getStPosY()
                        && ballPosY < stein.getStPosY()+40 && stein.getValue()>0){

                    stein.setValue( 0 );
                    ballRichtungY = -ballRichtungY-1;
                    score += 5;
                }
            }

            ballPosY += ballRichtungY * deltMillis / 08.0;
            if (ballPosY <= 1) {
                ballRichtungY = -ballRichtungY;

            }

            if (ballPosY > 750) {
                spiel = false;
                gameOver = true;
                ballRichtungX = 0;
                ballRichtungY = 0;
            }

            if (score >=40) {
                spiel = false;
                winner = true;
            }

        }

    }



    public void richtBewegen(long deltMillis) {
        spiel = true;
        spielerX += 20;
    }

    public void linksBewegen(long deltMillis) {
        spiel = true;
        spielerX -= 20;
    }

    // Gitter


    public boolean isWinner() {
        return winner;
    }

    public boolean isGameOver() {
        return gameOver;
    }

     public int getSpielerX() {
        final int spielerX = this.spielerX;
        return spielerX;
    }

    public int getScore() {
        return score;
    }

    public float getBallPosX() {


        return ballPosX;
    }

    public float getBallPosY() {

        return ballPosY;
    }

    public List<Stein> getSteine() {
        return steine;
    }

    }
