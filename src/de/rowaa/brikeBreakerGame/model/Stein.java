package de.rowaa.brikeBreakerGame.model;


public class Stein {

    private  int stPosX;
    private  int stPosY;
    private int value= 1;
    private int stWidth;
    private int stHeight;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Stein(int stPosX, int stPosY){

        this.stPosX = stPosX;
        this.stPosY = stPosY;
   }

    public void setStPosX(int stPosX) {
        this.stPosX = stPosX;
    }

    public void setStPosY(int stPosY) {
        this.stPosY = stPosY;
    }


    public int getStPosX() {
        return stPosX;
    }

    public int getStPosY() {
        return stPosY;
    }

    public void setStWidth(int stWidth) {
        this.stWidth = stWidth;
    }

    public void setStHeight(int stHeight) {
        this.stHeight = stHeight;
    }
}
