package de.rowaa.brikeBreakerGame;

import de.rowaa.brikeBreakerGame.model.Model;
import javafx.animation.AnimationTimer;

public class Timer extends AnimationTimer {

    private Model model;
    private Graphics graphics;

    long lastMillis = -1;

    public Timer(Model model, Graphics graphics){
        this.model = model;
        this.graphics= graphics;
    }


    @Override
    public void handle(long now) {

        long millis = now / 1000000;

        long deltMillis = 0;
        if (lastMillis !=-1) {

            deltMillis = millis - lastMillis;
        }


        this.model.update( deltMillis );

        lastMillis = millis;


         graphics.draw();
    }
}
