package de.rowaa.brikeBreakerGame;

import de.rowaa.brikeBreakerGame.model.Model;
import de.rowaa.brikeBreakerGame.model.Stein;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;


public class Graphics {

    private Image img = new Image( getClass().getResourceAsStream( "img1.jpg" ));
    private Image imageWinn = new Image( getClass().getResourceAsStream( "giphy.gif" ));
    private Image imageLoss = new Image( getClass().getResourceAsStream( "300w.gif" ));

    private Model model;
    private GraphicsContext gc;




    public Graphics(Model model, GraphicsContext gc){
        this.model = model;
        this.gc= gc;


    }



    public void draw() {
        gc.drawImage( img,0,0,1200,750 );

        //board
        gc.setFill( Color.MAGENTA );
        gc.fillRect( 0, 0, 3, 749 );
        gc.fillRect( 0, 0, 1199, 3 );
        gc.fillRect( 0, 0, 3, 749 );
        gc.fillRect( 1197, 0, 3, 749 );

        // Paddel
        gc.setFill( Color.AQUAMARINE );
        gc.fillRect

                ( model.getSpielerX(), 650, 200, 30);



//        steine

        for (Stein stein : model.getSteine()) {

            gc.setFill( Color.AQUA);

            if(stein.getValue() > 0) {
                gc.fillRect( stein.getStPosX(), stein.getStPosY(), 50, 30 );
            }
        }


        //Ball
        gc.setFill( Color.DEEPPINK );
        gc.fillOval( model.getBallPosX(), model.getBallPosY(), 25, 25 );


        // score

        gc.setFill( Color.YELLOW );
        gc.setFont( Font.font( "serif", FontPosture.ITALIC, 22 ) );
        gc.fillText( "Score: " + model.getScore(), 1050, 700 );


        //Gameover

        if (model.isGameOver()) {

            gc.drawImage( imageLoss,0,0,1200,750 );
            gc.setFill( Color.GHOSTWHITE );
            gc.fillRect( 360,270,475,170 );
            gc.setFill( Color.ORANGERED );
            gc.setFont( Font.font( "serif", FontPosture.ITALIC, 35 ) );
            gc.fillText( "Game Over", 520, 310 );
            gc.fillText( "Your Score: " + model.getScore(), 500, 360 );
            gc.fillText( "Press ENTER to restart", 440 , 410 );

             }



        if (model.isWinner()){
            gc.drawImage( imageWinn,0,0,1200,750 );
            gc.setFill( Color.HOTPINK );
            gc.fillRoundRect( 400,520,380,150,50,50 );
            gc.setFill( Color.WHITE );
            gc.setFont( Font.font( "serif", FontPosture.ITALIC, 35 ) );
            gc.fillText( "You Won", 530, 550 );
            gc.fillText( "Your Score: " + model.getScore(), 490, 600 );
            gc.fillText( "Press ENTER to restart", 420 , 650);


        }



        }
    }

